﻿    Console.OutputEncoding = System.Text.Encoding.UTF8; 
    Console.Write("Nhập vào số lượng các số Fibonacci cần in: ");
    int N = int.Parse(Console.ReadLine());

    int first = 0; 
    int second = 1;

    Console.Write(first + " " + second + " ");

    for(int i = 2; i < N; i++) {
      int next = first + second;
      Console.Write(next + " ");

      first = second;
      second = next;
    }
